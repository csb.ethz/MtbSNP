#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import argparse
import gurobipy as grb

from itertools import combinations


# Settings
###############################################################################

# Number of strains to leave out
k = 1

# Main
###############################################################################

p = argparse.ArgumentParser(description='Builds submodels from SNP model')
p.add_argument('id', help='Name of template model (currently only iNJ661)')
args = p.parse_args()

if args.id not in ['iNJ661']:
    raise ValueError('Did not recognize model {}'.format(args.id))

model = grb.read('models/snp/' + args.id + '_snp_model.mps')

strains = set(['N0004', 'N0052', 'N0054', 'N0072', 'N0091', 'N0136', 'N0145',
               'N0153', 'N0155', 'N0157', 'N1063', 'N1176', 'N1177', 'N1202',
               'N1216', 'N1272', 'N1274', 'N1283'])

for i, subset in enumerate(combinations(strains, k)):
    # Copy model
    submodel = model.copy()
    submodel.modelName = '_'.join([model.modelName] + sorted(subset))

    print()
    print(i, sorted(subset))
    subset = set(subset)

    # Remove or reformulate ratio constraints
    for c1 in list(submodel.getConstrs()):
        if c1.constrName.endswith(('_max', '_min')):
            x, s1 = c1.constrName.split('_')[-3:-1]

            if s1 in subset:
                submodel.remove(c1)
            elif x not in subset:
                continue

            for s2 in strains - subset:
                if s2 == s1:
                    continue

                c2 = submodel.getConstrByName(c1.constrName.replace(s1, s2))
                if not c2:
                    continue

                # Get rows of constraints
                r1 = submodel.getRow(c1)
                r2 = submodel.getRow(c2)

                # Combine constraints
                new_c = []
                for j in range(r1.size()):
                    v = r1.getVar(j)
                    if x in v.varName:
                        n = r1.getCoeff(j)
                    elif s1 in v.varName:
                        new_c.append((1, v))
                for j in range(r2.size()):
                    v = r2.getVar(j)
                    if x in v.varName:
                        n /= -r2.getCoeff(j)
                        break
                for j in range(r2.size()):
                    v = r2.getVar(j)
                    if s2 in v.varName:
                        new_c.append((n, v))

                new_id = c1.constrName.replace(x, s2)
                sense = '<' if c1.constrName.endswith('_max') else '>'
                submodel.addConstr(grb.LinExpr(new_c), sense, 0, new_id)

                break

    submodel.update()

    for strain in subset:
        # Remove strain constraints
        c = [x for x in submodel.getConstrs() if strain in x.constrName]
        for x in c:
            submodel.remove(x)

        # Remove strain variables
        v = [x for x in submodel.getVars() if strain in x.varName]
        for x in v:
            submodel.remove(x)

    submodel.update()

    # Save .lp and .mps files
    submodel.write('models/snp/submodels/' + submodel.modelName + '.lp')
    submodel.write('models/snp/submodels/' + submodel.modelName + '.mps')
