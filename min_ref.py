#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import argparse
import gurobipy as grb
import numpy as np
import tbx

# Gurobi parameters
param = {
    'OutputFlag': True,
    'Method': 2,
    'NumericFocus': 3,
    'CrossoverBasis': 1,
    'OptimalityTol': 1e-9,
    'FeasibilityTol': 1e-9
 }

# Parse arguments
p = argparse.ArgumentParser(description='Minimizes reference fluxes')
p.add_argument('model', action='store', type=str, help='SNP model')
args = p.parse_args()

print('Loading model...')

# Load model
if args.model.endswith('.lp') or args.model.endswith('.mps'):
    lp = grb.read(args.model)
    lp.setAttr('ModelSense', 1)
    for k, v in param.items():
        lp.setParam(k, v)
else:
    model = tbx.load_models(args.model)
    lp = tbx.create_lp(model, param=param, attr={'ModelSense': 1})

model_id = args.model.split('/')[-1].split('.')[0]

print('\nMinimizing reference fluxes...')

# Minimize reference fluxes
for v in lp.getVars():
    if '_ref_pos' in v.VarName:
        v.obj = 1
    elif '_ref_neg' in v.VarName:
        v.obj = -1
    else:
        v.obj = 0
lp.optimize()

if lp.status != 2:
    raise RuntimeError('Non-optimal solution status {}'.format(lp.status))

# Save solution
with open(model_id + '_ref_sol.csv', 'w') as f:
    for v in sorted(lp.getVars(), key=lambda x: x.VarName):
        f.write(','.join([v.VarName, str(v.X)]) + '\n')

# Constrain sum of reference fluxes
c = [(1, v) for v in lp.getVars() if '_ref_pos' in v.VarName]
c += [(-1, v) for v in lp.getVars() if '_ref_neg' in v.VarName]
b = np.sum([np.abs(v.X) for v in lp.getVars() if '_ref_' in v.VarName])
lp.addConstr(grb.LinExpr(c), '=', b, 'ref_sum_constr')

# Save problem
lp.update()
lp.write(model_id + '_ref.lp')
lp.write(model_id + '_ref.mps')
