#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import argparse
import pandas as pd
import tbx

# Settings
###############################################################################

bound = 100
solver = 'gurobi'
formats = ['json', 'sbml']

# Biomass reactions and components to remove
biomass = {'sMtb': 'BiomassGrowthInVitro', 'iNJ661': 'biomass_Mtb_9_60atp',
           'iNJ661m': 'biomass_Mtb_9_60atp_test_NOF', 'iSM810': 'BIOMASS',
           'GSMN_TB-Vtr': 'BIOMASSe'}
not_in_biomass = {'sMtb': [], 'iNJ661': ['glyc_c']}

# Main
###############################################################################

p = argparse.ArgumentParser(description='Builds strain-specific models')
p.add_argument('id', action='store', type=str,
               help='Name of template model (currently only iNJ661)')
args = p.parse_args()

if args.id not in ['iNJ661']:
    raise ValueError('Did not recognize model {}'.format(args.id))

template_path = 'models/template/' + args.id + '/' + args.id + '.xml'
kegg2model_path = 'data/KEGG_' + args.id + '.csv'
medium_path = 'data/experimental_medium_' + args.id + '.txt'
growth_rates_path = 'data/growth_rates_ci.csv'
deletions_path = 'data/deletions'
exchanges_path = 'data/reversibility_constraints_robust_export.csv'

print('Loading template model and data...')

template = tbx.load_model(template_path)
kegg2model, _ = tbx.load_kegg_mappings(kegg2model_path)
medium = tbx.load_medium(medium_path)
growth_rates = tbx.load_growth_rates(growth_rates_path)
deletions = tbx.load_deletions(deletions_path)
exchanges = tbx.load_exchanges(exchanges_path, kegg2model)

print('\nPreparing template model...')

template.solver = solver
tbx.prepare_template(template, biomass[template.id], medium, bound=bound,
                     not_in_biomass=not_in_biomass[template.id])

print('\nBuilding strain-specific models...')

models, data = tbx.build_strain_models(template, growth_rates, exchanges,
                                       deletions, bound=bound, solver=solver)

print('\nSaving models and data...')

tbx.save_models(models.values(), formats=formats)
pd.DataFrame(data).T.to_csv(template.id.split('_')[0] + '_model_data' + '.csv')
