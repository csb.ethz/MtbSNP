#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import argparse
import cobra
import gurobipy as grb
import numpy as np
import pandas as pd
import tbx

from warnings import warn
from collections import defaultdict


def align_reactions(reactions):
    """Maximizes reactant-product agreement by reversing reactions."""
    alignment = get_alignment(reactions)
    for r in alignment:
        if alignment[r] == -1:
            reverse_reaction(r)
    return alignment


def get_alignment(reactions):
    """Finds reaction directions that maximize reactant-product agreement."""
    alignment = {r: 1 for r in reactions}
    s = scores(alignment)
    for r in sorted(reactions, key=lambda r: r.reversibility, reverse=True):
        if s[r] <= 0:
            alignment[r] = -1
            s = scores(alignment)
            if s[r] <= 0:
                alignment[r] = 1
    return alignment


def scores(alignment):
    """Computes scores for reactions in alignment. A reaction's score is the
    sum of its weighted metabolite coefficients."""
    w = weights(alignment)
    return {r: sum(np.sign(c * d) * w[m] for m, c in r.metabolites.items())
            for r, d in alignment.items()}


def weights(alignment):
    """Returns weights for metabolites in alignment. A metabolite's weight is
    the difference between its number of positive and negative coefficients in
    reactions in the alignment. It is zero if the metabolite only occurs once.
    """
    p = defaultdict(int)
    n = defaultdict(int)
    for r, d in alignment.items():
        for m, c in r.metabolites.items():
            if c * d > 0:
                p[m] += 1
            else:
                n[m] += 1
    return {m: (p[m] + n[m] > 1) * (p[m] - n[m]) for m in set(p) | set(n)}


def reverse_reaction(reaction):
    """Reverses the stoichiometry and bounds of a reaction."""
    reaction.id += '_reverse'
    met_dict = {m: -2 * c for m, c in reaction.metabolites.items()}
    reaction.add_metabolites(met_dict)
    reaction.bounds = (-reaction.upper_bound, -reaction.lower_bound)


def sensitivity_analysis(cobra_model, disturbances, bound=1e3,
                         zero_cutoff=1e-6, param={}):
    """Returns loopless L2-minimal adjustments to given disturbances."""

    print(cobra_model.id)

    # Create Gurobi model and set default bounds
    model = tbx.create_lp(cobra_model, param=param)
    for v in model.getVars():
        v.lb, v.ub = -bound, bound

    # Remove objective and check feasibility
    model.ModelSense = 1
    model.setObjective(grb.LinExpr())
    model.optimize()
    if model.Status != 2:
        raise RuntimeError('Model {} is infeasible'.format(model.ModelName))

    # Remove blocked reactions (symmetric problem) and get remaining fluxes
    fluxes = []
    blocked = set()
    for v in model.getVars():
        v.obj = 1
        model.optimize()
        v.obj = 0
        if np.abs(v.x) < zero_cutoff:
            blocked.add(v.varName)
            model.remove(v)
        else:
            fluxes.append(v)
    model.update()

    # Get disturbances in Gurobi model
    deltas = [{model.getVarByName(r.id): d[r] for r in d
               if r.id not in blocked} for d in disturbances]

    # Get boundary fluxes
    boundary = [model.getVarByName(r.id) for r in cobra_model.reactions
                if r.id not in blocked and r.boundary]

    # Sensitivity matrix
    s = np.zeros((len(fluxes), len(deltas)))

    for i in range(len(deltas)):
        if i and not i % 100:
            print('{} / {}'.format(i, len(deltas)))

        # Copy sensitivities if same disturbance has already been analyzed
        j = deltas.index(deltas[i])
        if j < i:
            s[:, i] = s[:, j]
            continue

        while True:
            delta = deltas[i]

            if not any(delta.values()):
                break

            # Fix direction of disturbance
            for v in delta:
                if delta[v] >= 0:
                    v.lb = 0
                if delta[v] <= 0:
                    v.ub = 0

            # Minimize L2-distance to given disturbance
            d = delta.keys()
            obj = grb.QuadExpr()
            obj.addTerms([0.5] * len(d), d, d)
            obj.addTerms([-delta[v] for v in d], d)
            model.setObjective(obj)
            model.optimize()

            # Get new disturbance and reset bounds
            for v in delta:
                delta[v] = v.x if np.abs(v.x) > zero_cutoff else 0.0
                v.lb, v.ub = -bound, bound

            if not any(delta.values()):
                break

            # Scale disturbance by mean of non-zero absolute values
            n = np.mean(np.abs([x for x in delta.values() if x]))
            delta = {v: delta[v] / n for v in delta}

            # Set disturbance bounds
            for v in delta:
                v.lb = v.ub = delta[v]

            # Compute L2-minimal adjustment to disturbance
            obj = grb.QuadExpr()
            a = [v for v in fluxes if v not in delta]
            obj.addTerms([1.0] * len(a), a, a)
            model.setObjective(obj)
            model.optimize()

            # Reset disturbance bounds
            for v in delta:
                v.lb, v.ub = -bound, bound

            # If adjustment is a loop, disable it and retry
            if np.max(np.abs([v.x for v in boundary])) > zero_cutoff:
                break
            else:
                loop = {v: v.x for v in fluxes if np.abs(v.x) > zero_cutoff}
                # print(loop)
                disable_loop(model, loop, bound=bound)

        if not any(delta.values()):
            continue

        # Raise error if solution is not optimal
        if model.Status != 2:
            raise RuntimeError('No optimal adjustment found for {}'.format(
                {v.varName: delta[v] for v in delta}))

        # Check for sensitivities close to bounds
        bounded = [v.varName for v in set(fluxes) - set(delta)
                   if np.abs(v.x - v.lb) < 1.0 or np.abs(v.x - v.ub) < 1.0]
        if bounded:
            warn('Disturbance {} has bounded sensitivities: {}'.format(
                {v.varName: delta[v] for v in delta}, ', '.join(bounded)))

        # Round and save sensitivities
        s[:, i] = np.round([v.x for v in fluxes], int(-np.log10(zero_cutoff)))

    return pd.DataFrame(s, index=[v.varName for v in fluxes])


def disable_loop(model, loop, bound=1e3):
    """Disables a loop by adding binary variables and constraints."""

    balance = []

    for r in loop:
        # Add variables to model
        a = model.addVar(0, 1, 0, 'B', 'a_' + r.varName)
        g = model.addVar(-bound, bound, 0, 'C', 'G_' + r.varName)

        model.update()

        # Add constraints to model
        model.addConstr(r - bound * a >= -bound, a.varName + '_c1')
        model.addConstr(r - bound * a <= 0, a.varName + '_c2')
        model.addConstr(g + (bound + 1) * a >= 1, g.varName + '_c1')
        model.addConstr(g + (bound + 1) * a <= bound, g.varName + '_c2')

        # Add to loop balance
        balance.append((loop[r], g))

    # Add loop balance to model
    balance_id = 'loop_balance_' + '_'.join([r.varName for r in loop])
    model.addConstr(grb.LinExpr(balance), '=', 0, balance_id)


def remove_loops(model, fixed, solution=[]):
    """Removes loops from a solution based on CycleFreeFlux. Fluxed that should
    be fixed to specific values are given as an iterable of variables or IDs.
    Returns the loopless solution.
    """

    if not fixed:
        raise ValueError('No fixed fluxes')
    fixed = set(fixed)

    # Try using model solution if no other solution is provided
    if not solution:
        if model.status == 2:
            solution = [v.x for v in model.getVars()]
        else:
            raise ValueError('Model status not optimal')

    model.update()

    # Get original bounds
    bounds = {v: (v.lb, v.ub) for v in model.getVars()}

    # Set CycleFreeFlux bounds and objective
    model.ModelSense = 1
    model.setObjective(grb.LinExpr())
    for i, v in enumerate(model.getVars()):
        if v in fixed or v.varName in fixed or not solution[i]:
            v.lb = v.ub = solution[i]
        elif solution[i] > 0:
            v.lb = 0.0
            v.ub = solution[i]
            v.obj = 1.0
        elif solution[i] < 0:
            v.lb = solution[i]
            v.ub = 0.0
            v.obj = -1.0

    # Minimize L1-norm of fluxes
    model.optimize()

    # Reset bounds
    for v in model.getVars():
        v.lb, v.ub = bounds[v]

    return np.array([v.x for v in model.getVars()])


# Gurobi parameters
param = {
    'OutputFlag': False,
    # 'OptimalityTol': 1e-9,
    # 'FeasibilityTol': 1e-9,
    # 'IntFeasTol': 1e-9,
    'NumericFocus': 3
}

# Parse arguments
p = argparse.ArgumentParser(description='Gene-level sensitivity analysis')
p.add_argument('id', help='Name of template model (Currently only iNJ661)')
args = p.parse_args()

if args.id not in ['iNJ661']:
    raise ValueError('Did not recognize model {}'.format(args.id))

# Load template and strain models
temp = tbx.load_models('models/template/' + args.id, formats=['sbml']).pop()
models = tbx.load_models('models/strains/' + args.id + '/json')

# Get reaction alignments for all genes in template model
alignments = {g.id: get_alignment(g.reactions) for g in temp.genes}

for model in models:
    # Remove biomass reaction(s)
    bm = tbx.find_biomass_reactions(model)
    for r in bm:
        r.remove_from_model(remove_orphans=True)

    # Get disturbances from alignments
    disturbances = []
    reactions = set(r.id for r in model.reactions)
    for g in model.genes:
        a = {}
        for r, x in alignments[g.id].items():
            if r.id in reactions:
                a[model.reactions.get_by_id(r.id)] = x
        disturbances.append(a)

    # Compute sensitivities for gene-level perturbations
    s = sensitivity_analysis(model, disturbances, bound=1e3, param=param)

    # Save sensitivity matrix
    s.columns = [g.id for g in model.genes]
    s.to_csv(model.id + '_sensitivities.csv')
