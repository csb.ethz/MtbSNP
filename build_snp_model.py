#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

import argparse
import tbx

# Settings
###############################################################################

# Default flux bound for SNP model
bound = 100

# Cutoff for including sensitivities in SNP model
cutoff = 1e-5

# Output formats
formats = ['json', 'lp', 'mps']

# Main
###############################################################################

p = argparse.ArgumentParser(description='Builds SNP model from strain models')
p.add_argument('id', help='Name of template model (currently only iNJ661)')
args = p.parse_args()

if args.id not in ['iNJ661']:
    raise ValueError('Did not recognize model {}'.format(args.id))

models_path = 'models/strains/' + args.id + '/json'
kegg2model_path = 'data/KEGG_' + args.id + '.csv'
sensitivities_path = 'data/sensitivities/' + args.id
snps_path = 'data/missense_snps_strains.csv'
ratios_path = 'data/ratio_constraints_robust.csv'

print('Loading models and data...')

models = tbx.load_models(models_path)
kegg2model, _ = tbx.load_kegg_mappings(kegg2model_path)
snps = tbx.load_snps(snps_path)
ratios = tbx.load_exchange_ratios(ratios_path)
sensitivities = tbx.load_sensitivities(sensitivities_path)

print('\nBuilding SNP model...')

snp_model = tbx.build_snp_model(models, snps, ratios, sensitivities,
                                kegg2model, bound=bound, cutoff=cutoff)

print('\nSaving SNP model...')

tbx.save_models(snp_model, formats=formats)
