from __future__ import print_function, division

import cobra
import gurobipy as grb
import numpy as np
import pandas as pd

from cobra.flux_analysis import (flux_variability_analysis,
                                 single_gene_deletion,
                                 single_reaction_deletion)
from collections import defaultdict
from itertools import chain
from os import listdir
from os.path import isdir, isfile, join
from warnings import warn


def add_balances(snp_model, bound=100):
    """Adds a mass balance for each reaction flux in each strain with a
    reference flux variable that is the same for all strains. Optionally adds
    a variable representing an unexplained effect to each balance.
    """

    bm = find_biomass_reactions(snp_model)

    # Create reference reactions
    for r in list(snp_model.reactions):
        if r not in bm:
            ref_id = '_'.join(r.id.split('_')[1:]) + '_ref'
            if r.upper_bound > 0:
                try:
                    ref_pos = snp_model.reactions.get_by_id(ref_id + '_pos')
                    if r.upper_bound > ref_pos.upper_bound:
                        ref_pos.upper_bound = r.upper_bound
                except KeyError:
                    ref_pos = cobra.Reaction(ref_id + '_pos')
                    ref_pos.bounds = (0, r.upper_bound)
                    snp_model.add_reaction(ref_pos)
            if r.lower_bound < 0:
                try:
                    ref_neg = snp_model.reactions.get_by_id(ref_id + '_neg')
                    if r.lower_bound < ref_neg.lower_bound:
                        ref_neg.lower_bound = r.lower_bound
                except KeyError:
                    ref_neg = cobra.Reaction(ref_id + '_neg')
                    ref_neg.bounds = (r.lower_bound, 0)
                    snp_model.add_reaction(ref_neg)

    for r in list(snp_model.reactions):
        if r not in bm and '_ref_' not in r.id:
            # Create balance constraint
            c = cobra.Metabolite(r.id + '_balance')
            r.add_metabolites({c: 1})

            # Add reference fluxes to balance
            ref_id = '_'.join(r.id.split('_')[1:]) + '_ref'
            try:
                ref_pos = snp_model.reactions.get_by_id(ref_id + '_pos')
                ref_pos.add_metabolites({c: -1})
            except KeyError:
                pass
            try:
                ref_neg = snp_model.reactions.get_by_id(ref_id + '_neg')
                ref_neg.add_metabolites({c: -1})
            except KeyError:
                pass

            # Add unexplained effects to balance
            unex_pos = cobra.Reaction(r.id + '_unex_pos')
            unex_pos.bounds = 0, bound
            unex_neg = cobra.Reaction(r.id + '_unex_neg')
            unex_neg.bounds = -bound, 0
            unex_pos.add_metabolites({c: -1})
            unex_neg.add_metabolites({c: -1})
            snp_model.add_reactions([unex_pos, unex_neg])


def add_exchange_ratios(snp_model, ratios, kegg2model):
    """Adds exchange reaction ratio constraints to SNP model."""

    for k_ids in ratios:
        # Get model IDs
        m_ids = set(m for k in k_ids if k in kegg2model for m in kegg2model[k])
        if not m_ids:
            continue

        for s1 in ratios[k_ids]:
            # Get reference strain exchanges
            r1 = get_snp_model_boundary_reactions(snp_model, s1, m_ids)

            for s2 in ratios[k_ids][s1]:
                # Get non-reference strain exchanges
                r2 = get_snp_model_boundary_reactions(snp_model, s2, m_ids)

                # Get minimum and maximum ratios
                min_r, max_r = [float(x) for x in ratios[k_ids][s1][s2]]

                # Create exchange constraints
                c_id = '_'.join(sorted(list(m_ids)) + [s1, s2])
                if min_r:
                    c_min = cobra.Metabolite(c_id + '_min')
                    c_min._constraint_sense = 'G'
                    c_min._bound = 0
                    for r in r1:
                        r.add_metabolites({c_min: -min_r})
                    for r in r2:
                        r.add_metabolites({c_min: 1})
                if max_r:
                    c_max = cobra.Metabolite(c_id + '_max')
                    c_max._constraint_sense = 'L'
                    c_max._bound = 0
                    for r in r1:
                        r.add_metabolites({c_max: -max_r})
                    for r in r2:
                        r.add_metabolites({c_max: 1})


def add_snp_effects(snp_model, snps, sensitivities, bound=100, cutoff=1e-5):
    """Adds SNP effects to balances of affected reactions."""

    for snp, strains in snps.items():
        # Get affected gene
        try:
            g = snp_model.genes.get_by_id(snp.split('_')[0])
        except KeyError:
            continue

        # Filter affected strains
        strains = [s for s in strains
                   if s in sensitivities and g.id in sensitivities[s]]

        # Get IDs of directly affected reactions
        dir_r = set('_'.join(r.id.split('_')[1:]) for r in g.reactions)

        # Get feasible SNP effect signs and direct SNP effect bounds
        snp_signs = set([-1, 1])
        bounds = defaultdict(dict)
        for s in strains:
            for i, r_id in enumerate(sorted(dir_r)):
                # Get sensitivity
                try:
                    x = sensitivities[s][g.id][r_id]
                except KeyError:
                    continue
                if not x:
                    continue

                for snp_sign in list(snp_signs):
                    # Get reference flux
                    ref_sign = np.sign(-x * snp_sign)
                    ref_suffix = 'pos' if ref_sign > 0 else 'neg'
                    ref_id = '_'.join([r_id, 'ref', ref_suffix])
                    try:
                        ref = snp_model.reactions.get_by_id(ref_id)
                    except KeyError:
                        # Remove infeasible SNP effect sign
                        snp_signs.remove(snp_sign)
                        continue

                    # Get or create bound constraint
                    c_id = '_'.join([ref_id, 'dir_eff', s])
                    try:
                        c = snp_model.metabolites.get_by_id(c_id)
                    except KeyError:
                        c = cobra.Metabolite(c_id)
                        c._bound = 0
                        c._constraint_sense = 'G' if ref_sign > 0 else 'L'
                        ref.add_metabolites({c: 1})

                    bounds[snp_sign][c] = x

                if not snp_signs:
                    break
            if not snp_signs:
                break
        if not (snp_signs and np.all([bounds[x] for x in snp_signs])):
            continue

        # Get SNP effect coefficients in balances
        balances = {}
        for s in strains:
            for r_id, x in sensitivities[s][g.id].items():
                if np.abs(x) > cutoff:
                    # Add SNP sensitivity to balance
                    b_id = '_'.join([s, r_id, 'balance'])
                    try:
                        b = snp_model.metabolites.get_by_id(b_id)
                    except KeyError:
                        continue
                    balances[b] = -x

        # And SNP effects to bounds and balances
        for snp_sign in snp_signs:
            snp_suffix = 'pos' if snp_sign > 0 else 'neg'
            snp_eff = cobra.Reaction(snp + '_' + snp_suffix)
            snp_eff.bounds = sorted([0, snp_sign * bound])
            snp_eff.add_metabolites(bounds[snp_sign])
            snp_eff.add_metabolites(balances)
            snp_model.add_reaction(snp_eff)


def build_snp_model(models, snps, ratios, sensitivities, kegg2model, bound=100,
                    cutoff=1e-5):
    """Builds a model incorporating non-synononymous SNP effects and exchange
    ratio constraints inferred from dynamic exometabolomes for Mtb strains.

    All fluxes must satisfy a mass balance constraint that includes a reference
    flux (equal for all strains) and a strain-specific deviation from this
    reference flux consisting of SNP effects and unexplained effects.
    """

    # Create SNP model
    snp_model = cobra.Model('MtbSNP')

    print('\nAdding strain-specific models...')

    # Add strain-specific models to SNP model
    for model in models:
        # Change IDs of reactions and metabolites (genes shared by all strains)
        for x in chain(model.reactions, model.metabolites):
            x.id = '_'.join([model.id.split('_')[-1], x.id])
        model.repair()
        snp_model += model
    snp_model.repair()

    print('\nAdding balances...')

    # Add balances with reference fluxes and optionally unexplained effects
    add_balances(snp_model, bound=bound)

    print('\nAdding SNP effects...')

    # Add SNP effects to balances
    add_snp_effects(snp_model, snps, sensitivities, bound=bound, cutoff=cutoff)

    print('\nAdding exchange ratio constraints...')

    # Add exchange reaction ratio constraints
    add_exchange_ratios(snp_model, ratios, kegg2model)

    return snp_model


def build_strain_models(template, growth_rates, exchanges, deletions,
                        bound=100, zero_cutoff=1e-6, solver=None):
    """Builds strain-specific models by setting growth rates, setting bounds of
    boundary reactions to match exchanges inferred from dynamic exometabolomes,
    deleting genes, and setting theroretical bounds on C source uptake rates.
    """

    # Get intersection of strains in deletion and growth rate data
    strains = sorted(set(deletions.keys()) & set(growth_rates.keys()))

    models = {}
    data = {}

    # Estimated initial cell concentration [gDW / L]
    x0 = 1e-2

    # Initial C source concentrations [mmol / L]
    if 'iNJ661' in template.id:
        c0 = {'pyr_e': 36.35933647, 'glc__D_e': 11.10124334,
              'glu__L_e': 3.398355196, 'cit_e': 0.111972886}
    elif 'sMtb' in template.id:
        c0 = {'PYR_e': 36.35933647, 'GLC_e': 11.10124334,
              'GLU_e': 3.398355196, 'CIT_e': 0.111972886}
    else:
        raise ValueError('Model ID {} not recognized'.format(template.id))

    # Experimental durations [h]
    t = {'N0004': 193, 'N0052': 173, 'N0054': 172, 'N0072': 218,
         'N0091': 217, 'N0136': 169, 'N0145': 173, 'N0153': 194,
         'N0155': 180, 'N0157': 218, 'N1063': 145, 'N1176': 193,
         'N1177': 170, 'N1202': 170, 'N1216': 172, 'N1272': 169,
         'N1274': 172, 'N1283': 172}

    for s in strains:
        print('\nStrain:', s)
        model = template.copy()
        model.id = template.id.split('_')[0] + '_' + s

        # Set growth rate bounds
        bm = find_biomass_reactions(model).pop()
        bm.bounds = growth_rates[s]

        # Get medium metabolites and boundary reactions before setting bounds
        medium = get_medium_metabolites(model)
        boundary = get_boundary_reactions(model)

        # Set exchange reaction bounds
        set_exchanges(model, exchanges[s], bound=bound)

        check_export = []
        for r in boundary:
            m = r.reactants[0]

            # Allow import of medium components
            if m in medium:
                r.lower_bound = -bound

            if not ('C' in m.elements and 'H' in m.elements):
                # Allow export of simple waste products
                r.upper_bound = bound
            elif m.id[:-2] not in exchanges[s]:
                # Check if exports that are not in data are essential
                check_export.append(r)

        # Disallow non-essential exports that are not in data
        for r in check_export:
            r.upper_bound = 0
            if model.optimize().status != 'optimal':
                r.upper_bound = bound
                warn('Essential export not in data: {}'.format(r.id))

        # Set theoretical upper bounds for C source uptakes
        mu = growth_rates[s][0]
        uptake_bounds = set_uptake_bounds(model, mu, x0, c0, t[s])

        # Check feasibility
        solution = model.optimize()
        if solution.status != 'optimal':
            warn('Model is infeasible')
            continue

        # Delete non-essential genes
        deleted = filter_deletions(model, deletions[s])
        if deleted:
            cobra.manipulation.remove_genes(model, deleted)

        # Flux variability analysis
        var = flux_variability_analysis(model, fraction_of_optimum=0)

        # Set tight bounds and remove blocked reactions
        blocked = var[~np.any(var, axis=1)].index
        for r_id in var.index:
            r = model.reactions.get_by_id(r_id)
            if r.id in blocked:
                r.remove_from_model(remove_orphans=True)
            elif r.id != bm.id:
                r.lower_bound = var.loc[r.id]['minimum']
                r.upper_bound = var.loc[r.id]['maximum']

        # Check feasibility again
        solution = model.optimize()
        if solution.status != 'optimal':
            continue

        models[s] = model

        print('Solution status:', solution.status)
        print('Growth rate bounds:', bm.bounds)
        print('Uptake bounds:')
        for r in sorted(uptake_bounds, key=lambda x: x.id):
            print(r.id, uptake_bounds[r])
        print('Genes deleted:', len(deleted))
        print('Blocked reactions removed:', len(blocked))

        data[s] = {
            'template': template.id,
            'growth_rate_lb': bm.lower_bound,
            'growth_rate_ub': bm.upper_bound,
            'reactions': len(model.reactions),
            'metabolites': len(model.metabolites),
            'genes': len(model.genes),
            'exchanges': len(get_boundary_reactions(model)),
            'deletions': len(deleted),
            'blocked': len(blocked)
        }
        for r in uptake_bounds:
            data[s][r.id.split('_')[1].lower()] = np.abs(uptake_bounds[r])

    return models, data


def create_boundary_reaction(metabolite, bounds):
    """Creates a standard boundary reaction for the given metabolite. Does not
    add the boundary reaction to a model, not even if the metabolite is in one.

    Bounds are passed as an n x 2 iterable.
    """
    r = cobra.Reaction('EX_' + metabolite.id)
    r.add_metabolites({metabolite: -1})
    r.bounds = bounds
    return r


def create_lp(cobra_model, param={}, attr={}, scaling=False):
    """Creates a Gurobi LP model from a COBRA model."""

    # Conversions from COBRA to Gurobi
    type_dict = {'continuous': 'C', 'integer': 'I'}
    sense_dict = {'E': '=', 'L': '<', 'G': '>'}

    # Get objective
    obj = {r: 0 for r in cobra_model.reactions}
    obj.update(cobra.util.solver.linear_reaction_coefficients(cobra_model))

    # Create Gurobi model
    model = grb.Model(cobra_model.id)

    # Add variables and get constraints
    constr = {m: [] for m in cobra_model.metabolites if m._reaction}
    for r in cobra_model.reactions:
        v = model.addVar(r.lower_bound, r.upper_bound, obj[r],
                         type_dict[r.variable_kind], r.id)
        for m, c in r.metabolites.items():
            constr[m].append((c, v))

    model.update()

    # Add constraints
    for m in constr:
        bound = m._bound
        if scaling:
            # Scale coefficients
            w = [x[0] for x in constr[m]]
            scale = np.floor(np.log10(np.min(np.abs(w))))
            if scale:
                factor = np.power(10, -0.5 * scale)
                constr[m] = [(factor * x[0], x[1]) for x in constr[m]]
                bound *= factor
        c = grb.LinExpr(constr[m])
        model.addConstr(c, sense_dict[m._constraint_sense], bound, m.id)

    # Set parameters and attributes
    if 'OutputFlag' in param:
        model.setParam('OutputFlag', param['OutputFlag'])
    for k, v in param.items():
        if k != 'OutputFlag':
            model.setParam(k, v)
    for k, v in attr.items():
        model.setAttr(k, v)

    model.update()

    return model


def filter_deletions(model, deletions):
    """Returns a list of non-lethal gene deletions that are found in model."""
    model_genes = set(g.id for g in model.genes)
    gene_list = model_genes & set(deletions)
    lethal = find_essential_genes(model, gene_list=gene_list)
    nonlethal = gene_list - set(lethal)
    if lethal:
        warn('Skipped lethal gene deletions: {}'.format(', '.join(lethal)))
    return list(set(deletions) & nonlethal)


def find_biomass_reactions(model):
    """Returns probable biomass reactions in the model."""

    # Look for biomass in reaction and metabolite IDs
    reactions = set(r for r in model.reactions if 'biomass' in r.id.lower())
    for m in model.metabolites:
        if 'biomass' in m.id.lower():
            reactions |= set(r for r in m._reaction if not r.boundary)
    if reactions:
        return reactions

    # If method above failed, look for largest difference in metabolite counts
    met_count = [len(r.metabolites) for r in model.reactions]
    sorted_met_count = sorted(met_count)
    met_diff = list(np.diff(met_count))
    biomass_met_counts = sorted_met_count[met_diff.index(max(met_diff)) + 1:]
    return set(model.reactions[met_count.index(c)] for c in biomass_met_counts)


def find_essential_genes(model, gene_list=[], cutoff=1e-6):
    """Returns IDs of essential genes."""
    res = single_gene_deletion(model, gene_list=gene_list)
    return set(res[np.abs(res.growth) < cutoff].index)


def find_essential_reactions(model, reaction_list=None, cutoff=1e-6):
    """Returns IDs of essential reactions."""
    res = single_reaction_deletion(model, reaction_list=reaction_list)
    return set(res[np.abs(res.growth) < cutoff].index)


def get_boundary_dict(model):
    """Returns a dict of boundary reactions organized by metabolites."""
    return {r.reactants[0]: r for r in get_boundary_reactions(model)}


def get_boundary_reactions(model):
    """Returns a list of boundary reactions."""
    return [r for r in model.reactions if r.boundary]


def get_fixed_flux_reactions(model):
    """Returns a list of reactions with fixed flux."""
    return [r for r in model.reactions if r.lower_bound == r.upper_bound]


def get_medium_metabolites(model):
    """Returns a list of metabolites in medium."""
    return [m for m, r in get_boundary_dict(model).items()
            if r.lower_bound]


def get_model_paths(dir_path):
    """Returns a list of paths to .xml, .json, and .mat files in directory."""
    return [join(dir_path, f) for f in listdir(dir_path)
            if f.endswith('.xml') or f.endswith('.json') or f.endswith('.mat')]


def get_snp_model_boundary_reactions(snp_model, strain, metabolite_ids):
    """Returns boundary reactions of given metabolites in SNP model."""
    reactions = []
    for m_id in metabolite_ids:
        r_id = '_'.join([strain, 'EX', m_id])
        try:
            reactions.append(snp_model.reactions.get_by_id(r_id + '_e'))
        except KeyError:
            try:
                reactions.append(snp_model.reactions.get_by_id(r_id + '_c'))
            except KeyError:
                continue
    return reactions


def load_deletions(directory):
    """Reads lists of genes to be deleted from files in the given directory."""
    deletions = {}
    for x in listdir(directory):
        if x.startswith('N'):
            p = join(directory, x)
            if isfile(p):
                with open(p, 'r') as f:
                    deletions[x.split('_')[0]] = [l.strip() for l in f]
    return deletions


def load_exchanges(exchange_file, kegg2model):
    """Loads metabolite exchanges."""

    # Exchange patterns corresponding to reversibility values
    patterns = {0: [0, 0], 1: [0, 1], 2: [-1, 0], 3: [-1, 1]}

    with open(exchange_file, 'r') as f:
        strains = f.readline().strip().split(',')[1:]
        exchanges = {s: {} for s in strains}

        # Set of metabolites with multiple exchanges
        multiple = set()

        for l in [line.strip().split(',') for line in f]:
            for kegg_id in [k for k in l.pop(0).split(';')]:
                try:
                    model_ids = kegg2model[kegg_id]
                except KeyError:
                    continue

                for m_id in model_ids:
                    for i, p in enumerate([patterns[int(x)] for x in l]):
                        # Use largest bounds if multiple exchanges
                        if m_id in exchanges[strains[i]]:
                            lb, ub = exchanges[strains[i]][m_id]
                            if (lb, ub) != p:
                                p = [min(lb, p[0]), max(ub, p[1])]
                                multiple.add(m_id)

                        exchanges[strains[i]][m_id] = p

    if multiple:
        warn('Multiple exchanges: {}'.format(', '.join(sorted(multiple))))

    return exchanges


def load_growth_rates(growth_rates_file):
    """Returns a dict of growth rate ranges."""
    growth_rates = {}
    with open(growth_rates_file, 'r') as f:
        f.readline()
        for line in f:
            strain, _, lb, ub, _ = line.strip().split(',')
            growth_rates[strain] = (float(lb), float(ub))
    return growth_rates


def load_kegg_mappings(kegg_file):
    """Loads metabolite mappings between KEGG and model."""
    kegg2model = {}
    model2kegg = {}
    with open(kegg_file, 'r') as f:
        for line in f:
            model_id, kegg_ids = line.strip().split(',')
            model2kegg[model_id] = kegg_ids.split(';')
            for kegg_id in model2kegg[model_id]:
                try:
                    kegg2model[kegg_id].append(model_id)
                except KeyError:
                    kegg2model[kegg_id] = [model_id]
    return kegg2model, model2kegg


def load_medium(medium_file):
    """Returns growth medium as a set."""
    with open(medium_file, 'r') as f:
        return set(line.strip() + '_e' for line in f)


def load_model(path):
    """Loads a single model from SBML, JSON, or MATLAB file."""
    return load_models(path).pop()


def load_models(paths, formats=[]):
    """ Loads multiple models from .xml, .json, or .mat files. Accepts a list
    of paths to model files and/or directories containing model files.
    """

    if isinstance(paths, str):
        paths = [paths]

    models = []

    while paths:
        path = paths.pop(0)
        if isdir(path):
            paths.extend(get_model_paths(path))
            continue

        if path.endswith('.xml') and (not formats or 'sbml' in formats):
            try:
                model = cobra.io.read_sbml_model(path)
            except cobra.io.sbml3.CobraSBMLError:
                warn('Failed to read {} as SBML file'.format(path))
                continue
        elif path.endswith('.json') and (not formats or 'json' in formats):
            try:
                model = cobra.io.load_json_model(path)
            except ValueError:
                warn('Failed to read {} as JSON file'.format(path))
                continue
        elif path.endswith('.mat') and (not formats or 'mat' in formats):
            try:
                model = cobra.io.load_matlab_model(path)
            except ValueError:
                warn('Failed to read {} as MATLAB file'.format(path))
                continue
        else:
            continue

        model.id = ''.join(path.split('.')[0]).split('/')[-1]
        models.append(model)

    return models


def load_exchange_ratios(ratios_file):
    """Loads exchange reaction ratios."""

    ratios = {}

    with open(ratios_file, 'r') as f:
        for l in [line.strip().split(',') for line in f]:
            # Get sorted tuple of KEGG IDs
            kegg_ids = tuple(sorted(k for k in l.pop(0).split(';')))

            # Get strains and ratios
            s, ref, min_r, max_r = l

            # Save ratio constraint
            if kegg_ids not in ratios:
                ratios[kegg_ids] = {ref: {}}
            elif s in ratios[kegg_ids][ref]:
                warn('Multiple {} / {} ratios for {}'.format(s, ref, kegg_ids))
            ratios[kegg_ids][ref].update({s: (min_r, max_r)})

    return ratios


def load_sensitivities(path):
    """Loads gene-level structural sensitivities from directory."""
    return {x.split('_')[-2]: pd.read_csv(join(path, x), index_col=0).to_dict()
            for x in listdir(path) if isfile(join(path, x))}


def load_snps(path, genes=[]):
    """Loads SNP data from file."""
    snps = {}
    with open(path, 'r') as f:
        strains = f.readline().strip().split(',')[1:]
        for l in [line.strip().split(',') for line in f]:
            snps[l[0]] = [strains[i] for i, x in enumerate(l[1:]) if int(x)]
    return snps


def load_solution(solution_file):
    """Loads a flux distribution from file."""
    solution = {}
    with open(solution_file, 'r') as f:
        for l in f:
            x = l.strip().split(',')
            solution[x[0]] = float(x[1])
    return solution


def load_variabilities(variability_file):
    """Loads SNP effect variabilities."""
    var = {}
    with open(variability_file, 'r') as f:
        for l in [line.strip().split(',') for line in f]:
            var[l[0]] = (float(l[1]), float(l[2]))
    return var


def prepare_template(model, biomass, medium, bound=100, not_in_biomass=[]):
    """Replaces boundary reactions, removes fixed-flux reactions, sets single
    biomass reaction as objective, sets growth medium, removes selected biomass
    components, and sets default bound.

    In-place modification of the original model.
    """

    # Set single biomass reaction as objective
    try:
        bm = model.reactions.get_by_id(biomass)
    except KeyError:
        if biomass in model.reactions:
            bm = biomass
        else:
            raise ValueError('Invalid biomass reaction')
    model.objective = {bm: 1}
    bm.bounds = 0, bound

    # Subtract selected biomass components
    for m, c in bm.metabolites.items():
        if m.id in set(not_in_biomass):
            bm.subtract_metabolites({m: c})

    # Delete any other biomass reactions
    for r in find_biomass_reactions(model):
        if not r.objective_coefficient:
            r.delete(remove_orphans=True)

    # Delete boundary and fixed-flux reactions
    for r in get_boundary_reactions(model):
        r.delete(remove_orphans=True)
    for r in get_fixed_flux_reactions(model):
        r.delete(remove_orphans=True)

    # Set default bounds for remaining reactions
    for r in model.reactions:
        if r.lower_bound < 0:
            r.lower_bound = -bound
        if r.upper_bound > 0:
            r.upper_bound = bound

    # Add boundary reactions for extracellular metabolites and set medium
    boundary = [create_boundary_reaction(m, [-bound * (m.id in medium), bound])
                for m in model.metabolites if m.compartment == 'e']
    model.add_reactions(boundary)


def save_models(models, formats=[]):
    """Saves models (supports .xml, .json, .mat, .lp, and .mps format). If no
    formats are specified, the model is saved as .xml).
    """
    if isinstance(models, cobra.Model):
        models = [models]
    if isinstance(formats, str):
        formats = [formats]
    for model in models:
        if not formats or 'sbml' in formats or 'xml' in formats:
            cobra.io.write_sbml_model(model, model.id + '.xml')
        if 'json' in formats:
            cobra.io.save_json_model(model, model.id + '.json')
        if 'mat' in formats:
            cobra.io.save_matlab_model(model, model.id + '.mat')
        if 'lp' in formats or 'mps' in formats:
            lp = create_lp(model)
            if 'lp' in formats:
                lp.write(lp.ModelName + '.lp')
            if 'mps' in formats:
                lp.write(lp.ModelName + '.mps')


def set_exchanges(model, exchanges, bound=100):
    """Sets the bounds of boundary reactions in model to match exchanges from
    dynamic exometabolomes. New boundary reactions are added when necessary.
    """
    for m_id, pattern in exchanges.items():
        # Get bounds from pattern
        bounds = [x * bound for x in pattern]
        try:
            model.reactions.get_by_id('EX_' + m_id + '_e').bounds = bounds
        except KeyError:
            try:
                model.reactions.get_by_id('EX_' + m_id + '_c').bounds = bounds
            except KeyError:
                try:
                    # Check if cytoplasmic metabolite exists in model
                    m = model.metabolites.get_by_id(m_id + '_c')
                    if np.any(bounds):
                        # Add new boundary reaction to model
                        model.add_reaction(create_boundary_reaction(m, bounds))
                except KeyError:
                    continue


def set_uptake_bounds(model, mu, x0, c0, t):
    """Sets theoretical bounds for uptake reactions using growth rate, initial
    cell and C source concentrations, and experimental duration.
    """
    bounds = {}
    for m in c0:
        try:
            r = model.reactions.get_by_id('EX_' + m)
            if r.lower_bound:
                r.lower_bound = mu * c0[m] / (x0 * (1 - np.exp(mu * t)))
                bounds[r] = r.lower_bound
        except KeyError:
            continue
    return bounds
