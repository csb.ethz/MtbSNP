![MtbSNP](figures/mtbsnp.png)

# MtbSNP

MtbSNP is a constraint-based model of genetic variation and its effects on metabolism in clinical strains of *Mycobacterium tuberculosis*. It can be used to predict the metabolic effects of non-synonymous single-nucleotide polymorphisms (SNPs) in enzyme-encoding genes, and these predictions allow classification of functional SNPs.

For detailed information, please refer to our paper [1].
 
## Files

The MtbSNP model is provided for convenience (in .json, .mps, and .lp format) and this repository contains all the Python code and data needed to build and solve it. This can be done using the provided Python scripts as described below. Custom functions required by these scripts are found in `tbx.py`.

## Requirements

To run the code in this repository you will need Python (tested with version 3.7.0 but should work with other versions as well) and the Gurobi Optimizer (tested with versions >=7.5.2).

Models are built using cobrapy (tested with version 0.13.4, currently does not work with the latest version) and solved using the Gurobi Python interface gurobipy. A complete list of dependencies can be found in `requirements.txt`.

## Instructions

The results from our paper can be reproduced by following the steps below.

### Building the model

![Model overview](figures/model.png)

First, construct strain-specific genome-scale metabolic models from a template model (currently only iNJ661 [2] is guaranteed to work):
```
python build_strain_models.py iNJ661
```
This produces a strain-specific version of the template model by setting experimental growth rates, setting bounds of boundary reactions to match exchanges inferred from dynamic exometabolomes, deleting genes, and setting theroretical bounds on carbon source uptake rates.

Second, apply gene-level structural sensitivity analysis to the strain-specific models:
```
python sensitivity.py iNJ661
```
The sensitivities obtained from this analysis are used to define the metabolic effects of non-synonymous SNPs.

Finally, build the MtbSNP model using strain-specific models, SNPs, sensitivities, and metabolite exchange ratios:
```
python build_snp_model.py iNJ661
```
This final step can take some time.


### Solving the model

![Solution procedure](figures/solution.png)

First, minimize the unexplained flux effects using `min_unex.py` and the MtbSNP model in one of the supported formats (.xml, .mat, .json, .mps, or .lp):
```
python min_unex.py MtbSNP.mps
```
This produces a new model, MtbSNP_unex (in .mps and .lp format), in which unexplained effects are constrained to their minimum.

Second, minimize the reference fluxes using `min_ref.py`:
```
python min_ref.py MtbSNP_unex.mps
```
This produces a new model, MtbSNP_unex_ref (in .mps and .lp format), in which unexplained effects and reference fluxes are constrained to their respective minima. The solution is used to obtain relative SNP effects.

Finally, minimize each SNP effect using `min_var.py`:
```
python min_var.py MtbSNP_unex.mps Rv1617_E220D_neg
```
Here, the effect of the SNP causing amino acid change E220D in the enzyme encoded by gene Rv1617 (*pykA*) is minimized in MtbSNP_unex. This can also be done using MtbSNP_unex_ref, but the additional constraint makes each problem harder to solve. The minimal SNP effects are used to classify SNPs as functional or non-functional.

### Submodel analysis

The script `build_snp_submodels.py` builds all possible submodels of MtbSNP in which *k* strains have been removed:
```
python build_snp_submodels.py iNJ661
```
These models can be solved as described above for the full model to evaluate the sensitivity of the predicted SNP effects and functional SNP classifications.

### References

[1] O. Øyås, S. Borrell, A. Trauner, M. Zimmermann, J. Feldmann, T. Liphardt, S. Gagneux, J. Stelling, U. Sauer, and M. Zampieri. "Model-based integration of genomics and metabolomics reveals SNP functionality in *Mycobacterium tuberculosis*". *Proceedings of the National Academy of Sciences* (2020).

[2] N. Jamshidi and B. Palsson. "Investigating the metabolic capabilities of *Mycobacterium tuberculosis* H37Rv using the *in silico* strain iNJ661 and proposing alternative drug targets". *BMC Systems Biology* 1.26 (2007).
