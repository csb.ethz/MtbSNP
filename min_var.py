#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import gurobipy as grb
from warnings import warn

# Parse arguments
p = argparse.ArgumentParser(description='Minimizes a single variable')
p.add_argument('lp', action='store', type=str, help='SNP model (.lp or .mps)')
p.add_argument('var', action='store', type=str, help='Variable name')
args = p.parse_args()

# Read LP
lp = grb.read(args.lp)

# Set parameters
param = {
    'OutputFlag': True,
    'Method': 2,
    'NumericFocus': 3,
    'CrossoverBasis': 1,
    'OptimalityTol': 1e-9,
    'FeasibilityTol': 1e-9,
    'Threads': 1
}
for p, v in param.iteritems():
    lp.setParam(p, v)

# Remove objective
for x in lp.getVars():
    x.obj = 0.0

# Get variable
v = lp.getVarByName(args.var)

# Minimize
v.obj = 1
lp.ModelSense = -1 if args.var.endswith('_neg') else 1
lp.optimize()

if lp.status == 2:
    # Write minimum to file
    model_id = lp_file.split('/')[-1].split('.')[0]
    with open(v.varName + '_' + model_id + '.txt', 'w') as f:
        f.write(','.join([v.varName, str(v.x)]) + '\n')
else:
    raise RuntimeError('Solution not optimal')
